from apscheduler.scheduler import Scheduler

from my_module import do_work_often, do_some_more_work

sched = Scheduler()

@sched.interval_schedule(hours=1)
def first_job():
    do_work_often()

@sched.cron_schedule(day_of_week='mon-fri', hour=1)
def second_job():
    do_some_more_work()

sched.start()

while True:
pass
